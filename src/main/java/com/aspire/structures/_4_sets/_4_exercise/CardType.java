package com.aspire.structures._4_sets._4_exercise;

public enum CardType {
    SPADES,
    DIAMONDS,
    HEARTS
}